from solcast_to_influx import Solcast_API
import time
import schedule
import datetime

def launch_app():
    ts = datetime.datetime.now().timestamp()
    ts_converted = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(ts))
    print(f'{ts_converted}: Starting the app, please wait...')

    # get data from Solcast API
    solcast = Solcast_API()
    df = solcast.request_solcast_api()
        
    # ingest data into Influxdb
    solcast.ingest_influx(df)   

schedule.every().hour.at(":00").do(launch_app)
schedule.every().hour.at(":30").do(launch_app)

if __name__ == '__main__':
    while True:
        schedule.run_pending()
        time.sleep(1)