from influxdb_client.client.write_api import SYNCHRONOUS
import pandas as pd
import requests
import configparser
from influxdb_client import InfluxDBClient, WriteOptions
from json.decoder import JSONDecodeError
import time, datetime

class Solcast_API():

    def __init__(self):
        # set the configuration parser for solcast and influx config data
        config = configparser.ConfigParser()
        config.read('./config/config.ini')

        # get configuration variables
            # solcast config
        self.solcast_url = config['solcast']['url']
        self.solcast_token = {'Authorization': 'Bearer ' + config['solcast']['headers']}
            # influx config
        self.influx_url = config['influx2']['url']
        self.influx_org = config['influx2']['org']
        self.influx_token = config['influx2']['token']
        self.bucket = config['influx2']['bucket']

    def request_solcast_api(self):
        try:
            # get data from solcast api
            ts = datetime.datetime.now().timestamp()
            ts_converted = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(ts))
            print(f'{ts_converted}: Starting request to Solcast...')
            r = requests.get(self.solcast_url, headers=self.solcast_token)
            
            if r.status_code == 200:
                try:
                    ts = datetime.datetime.now().timestamp()
                    ts_converted = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(ts))
                    print(f'{ts_converted}: Solcast request completed.')
                    # get the JSON from the request
                    payload = r.json()

                    # pass it to a pandas DataFrame
                    df = pd.json_normalize(payload['estimated_actuals'])
                    df = df.set_index('period_end')

                    return df
                except JSONDecodeError:
                    print('ERROR: JSON decoding error')
                    return None
            else:
                print(f'ERROR: An error occured during the request. Status code: {r.status_code}')
                return None

        except requests.RequestException:
            print(f'ERROR: An error occured during the GET request. Status code: {r.status_code}')
            return None

    def ingest_influx(self, df):
            
        # write data into influx
        try:
            with InfluxDBClient(url=self.influx_url, \
                                token=self.influx_token, \
                                org=self.influx_org) \
                                as _client:

                with _client.write_api(write_options=SYNCHRONOUS) as _write_client:
                    # write pandas dataframe
                    bucket = self.bucket
                    _data_frame = df

                    try:
                        _write_client.write(bucket, 
                                            self.influx_org, 
                                            record=_data_frame, 
                                            data_frame_measurement_name='pv_actuals',
                                            data_frame_tag_columns=['period']
                                            )
                        ts = datetime.datetime.now().timestamp()
                        ts_converted = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(ts))
                        print(f'{ts_converted}: Data ingest successfully done in bucket {bucket} of Influx OSS 2.0')  
                        _client.close()  
                        return None
                    except Exception:
                        ts = datetime.datetime.now().timestamp()
                        ts_converted = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(ts))
                        print(f'ERROR: Connection error while trying to connect to InfluxDB OSS 2.0 at {ts_converted}')
                        _client.close()
                        return None

        except ConnectionError as c:
            print(c)