FROM python:3.8-slim

ENV TZ=Europe/Madrid

RUN mkdir pv_solcast/

COPY . /pv_solcast
WORKDIR /pv_solcast

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN pip install schedule==1.0.0
RUN pip install influxdb-client==1.18.0
RUN pip install pandas==1.2.1
RUN pip install requests==2.25.1

EXPOSE 5000

CMD ["python", "-u", "./__main__.py"]